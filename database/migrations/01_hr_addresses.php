<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_addresses', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['home','work']);
            $table->integer('ref_id')->nullable();
            $table->integer('country_id');
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('street')->nullable();
            $table->string('post_code')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('description')->nullable();
            $table->string('tenant_id');
            $table->string('company_id');
            $table->string('branch_id');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_addresses');
    }
};
