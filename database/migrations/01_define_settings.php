<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('define_settings', function (Blueprint $table) {
            $table->id();
            $table->string('setting_name');
            $table->string('setting_class')->nullable();
            $table->string('label');
            $table->string('value');
            $table->string('lang');
            $table->string('tenant_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('define_settings');
    }
};
