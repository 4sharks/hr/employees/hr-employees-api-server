<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_locations', function (Blueprint $table) {
            $table->id();
            $table->integer('address_id')->nullable();
            $table->string('name');
            $table->boolean('have_fingerprint')->default(false);
            $table->string('ip_fingerprint')->nullable();
            $table->string('description')->nullable();
            $table->string('tenant_id');
            $table->string('company_id');
            $table->string('branch_id');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_locations');
    }
};
