<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_employees_resume_lines', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id');
            $table->string('title');
            $table->enum('type',['education','experience']);
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->string('description')->nullable();
            $table->string('created_by');
            $table->string('tenant_id');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_employees_resume_lines');
    }
};
