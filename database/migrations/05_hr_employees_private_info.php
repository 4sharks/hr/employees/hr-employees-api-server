<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_employees_private_info', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('emergency_person_name')->nullable();
            $table->string('emergency_person_phone')->nullable();
            $table->enum('gender',['male','female'])->nullable();
            $table->integer('nationality_id')->nullable();
            $table->string('identification_id')->nullable();
            $table->string('passport_id')->nullable();
            $table->date('dob')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('country_of_birth')->nullable();
            $table->string('home_address_id')->nullable();
            $table->string('home_work_distance')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('number_of_children')->nullable();
            $table->integer('bank_account_id')->nullable();
            $table->string('main_language')->nullable();
            $table->string('certificate_level_id')->nullable();
            $table->string('field_of_study')->nullable();
            $table->string('school_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_employees_private_info');
    }
};
