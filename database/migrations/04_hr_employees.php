<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_employees', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_no')->nullable();
            $table->string('name_en');
            $table->string('name_ar')->nullable();
            $table->integer('job_position_id')->nullable();
            $table->integer('employee_type_id')->nullable();
            $table->string('tags')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('manager_id')->nullable();
            $table->integer('coach_id')->nullable();
            $table->string('pic')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('work_mobile')->nullable();
            $table->string('work_email')->nullable();
            $table->integer('work_location_id')->nullable();
            $table->integer('approvers_expense')->nullable();
            $table->integer('approvers_timeoff')->nullable();
            $table->integer('working_hours_id')->nullable();
            $table->integer('timezone')->nullable();
            $table->string('related_user_id')->nullable();
            $table->integer('current_contract_id')->nullable();
            $table->integer('pin_code')->nullable();
            $table->integer('badge_id')->nullable();
            $table->string('tenant_id');
            $table->string('company_id');
            $table->string('branch_id');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_employees');
    }
};
