<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_employee_resume_skills', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id');
            $table->integer('skill_type_id');
            $table->integer('skill_id');
            $table->integer('skill_level_id');
            $table->string('created_by');
            $table->string('tenant_id');
            $table->string('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_employee_resume_skills');
    }
};
