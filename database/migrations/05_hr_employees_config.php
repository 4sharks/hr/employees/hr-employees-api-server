<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_employees_config', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id');
            $table->string('emp_type');
            $table->string('emp_related_user_id');
            $table->integer('current_contract_id');
            $table->string('pincode');
            $table->string('badge_id');
            $table->boolean('is_manager');
            $table->boolean('is_trainer');
            $table->boolean('is_approval_expense');
            $table->boolean('is_approval_timeoff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_employees_config');
    }
};
