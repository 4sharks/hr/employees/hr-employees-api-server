<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hr_job_position', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('department_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('employee_type_id')->nullable();
            $table->boolean('is_published')->default(false);
            $table->integer('target')->nullable();
            $table->integer('recruiter_id')->nullable();
            $table->string('intervieews_ids')->nullable();
            $table->string('job_summary')->nullable();
            $table->string('tenant_id');
            $table->string('company_id');
            $table->string('branch_id');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hr_job_position');
    }
};
