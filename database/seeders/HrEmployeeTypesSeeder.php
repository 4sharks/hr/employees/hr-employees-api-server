<?php

namespace Database\Seeders;

use App\Models\HrEmployeeTypeModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HrEmployeeTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $_data = [
            [
                'name' => 'employee',
            ],
            [
                'name' => 'student',
            ],
            [
                'name' => 'trainer',
            ],
            [
                'name' => 'contractor',
            ],
            [
                'name' => 'freelancer',
            ],
        ];

        HrEmployeeTypeModel::insert($_data);
    }
}
