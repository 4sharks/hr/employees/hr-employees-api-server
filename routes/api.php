<?php

use App\Http\Controllers\DefineSettingController;
use App\Http\Controllers\HrCountriesController;
use App\Http\Controllers\HRDepartmentController;
use App\Http\Controllers\HrEmployeeResumeSkillController;
use App\Http\Controllers\HREmployeesController;
use App\Http\Controllers\HrEmployeesResumeLineController;
use App\Http\Controllers\HrEmployeeTypesController;
use App\Http\Controllers\HrJobPositionController;
use App\Http\Controllers\HRLocationsController;
use App\Http\Controllers\HrSkillsLevelsController;
use App\Http\Controllers\HrSkillsTypesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware(['cors'])->group(function () {
    Route::post('/hr/locations/add', [HRLocationsController::class,'store']);
    Route::put('/hr/locations/{id}', [HRLocationsController::class,'update']);
    Route::post('/hr/locations', [HRLocationsController::class,'getLocations']);
    Route::delete('/hr/locations/{id}', [HRLocationsController::class,'delete']);

    Route::post('/hr/employees', [HREmployeesController::class,'getEmployees']);
    Route::post('/hr/employees/dept', [HREmployeesController::class,'getEmployeesByDept']);
    Route::post('/hr/employees/add', [HREmployeesController::class,'store']);
    Route::put('/hr/employees/update/{id}', [HREmployeesController::class,'update']);
    Route::delete('/hr/employees/{id}', [HREmployeesController::class,'delete']);
    Route::get('/hr/employees/{tenantId}/{companyId}/{branchId}/{id}', [HREmployeesController::class,'getEmployeeById']);
    Route::get('/hr/employees/user/{userId}', [HREmployeesController::class,'getEmployeeByUserId']);

    Route::post('/hr/departments', [HRDepartmentController::class,'getDepartments']);
    Route::post('/hr/departments/add', [HRDepartmentController::class,'store']);
    Route::put('/hr/departments/{id}', [HRDepartmentController::class,'update']);
    Route::delete('/hr/departments/{id}', [HRDepartmentController::class,'delete']);

    Route::post('/hr/job-positions', [HrJobPositionController::class,'getJobPositions']);
    Route::post('/hr/job-positions/add', [HrJobPositionController::class,'store']);
    Route::put('/hr/job-positions/{id}', [HrJobPositionController::class,'update']);
    Route::delete('/hr/job-positions/{id}', [HrJobPositionController::class,'delete']);

    Route::get('hr/employee-types',[HrEmployeeTypesController::class,'getEmployeeTypes']);
    Route::get('hr/countries',[HrCountriesController::class,'getAll']);

    Route::post('hr/define-settings',[DefineSettingController::class,'store']);
    Route::put('hr/define-settings/{id}',[DefineSettingController::class,'update']);
    Route::get('hr/define-settings/{tenant_id}/{company_id}/{setting_name}',[DefineSettingController::class,'showAll']);
    Route::get('hr/define-settings/{id}',[DefineSettingController::class,'show']);
    Route::delete('hr/define-settings/{id}',[DefineSettingController::class,'delete']);

    Route::post('hr/resume-lines',[HrEmployeesResumeLineController::class,'store']);
    Route::put('hr/resume-lines/{id}',[HrEmployeesResumeLineController::class,'update']);
    Route::get('hr/resume-lines/{tenant_id}/{company_id}',[HrEmployeesResumeLineController::class,'showAll']);
    Route::get('hr/resume-lines/{id}',[HrEmployeesResumeLineController::class,'show']);
    Route::delete('hr/resume-lines/{id}',[HrEmployeesResumeLineController::class,'delete']);

    Route::post('hr/skills-type',[HrSkillsTypesController::class,'store']);
    Route::get('hr/skills-type/index/{tenant_id}/{company_id}',[HrSkillsTypesController::class,'index']);
    Route::post('hr/skills-type/all',[HrSkillsTypesController::class,'storeAll']);
    Route::put('hr/skills-type/{id}',[HrSkillsTypesController::class,'update']);
    Route::get('hr/skills-type/{tenant_id}/{company_id}',[HrSkillsTypesController::class,'showAll']);
    Route::get('hr/skills-type/{id}',[HrSkillsTypesController::class,'show']);
    Route::delete('hr/skills-type/{id}',[HrSkillsTypesController::class,'delete']);

    Route::post('hr/skills-levels',[HrSkillsLevelsController::class,'store']);
    Route::put('hr/skills-levels/{id}',[HrSkillsLevelsController::class,'update']);
    Route::get('hr/skills-levels/{tenant_id}/{company_id}',[HrSkillsLevelsController::class,'showAll']);
    Route::get('hr/skills-levels/type/{tenant_id}/{company_id}/{type_id}',[HrSkillsLevelsController::class,'showByTypeId']);
    Route::get('hr/skills-levels/{id}',[HrSkillsLevelsController::class,'show']);
    Route::delete('hr/skills-levels/{id}',[HrSkillsLevelsController::class,'delete']);

    Route::post('hr/resume-skills',[HrEmployeeResumeSkillController::class,'store']);
    Route::get('hr/resume-skills/{tenant_id}/{company_id}/{emp_id}',[HrEmployeeResumeSkillController::class,'showAll']);

// });
