<?php
namespace App\Classes;

class Employee {

    /**
     * employee number
     * @var string
     */
    protected $emp_no;

    /**
     * employee arabic name
     * @var string
     */
    protected $name_en;

    /**
     * employee english name
     * @var string
     */
    protected $name_ar;

    /**
     * job posiiton id
     * @var integer|null
     */
    protected $job_position_id;

    /**
     * employee type
     * @var integer|null
     */
    protected $employee_type_id;

    /**
     * employee tags to filter employees by their
     * @var array|null
     */
    protected $tags;

    /**
     * employee department id
     * @var integer|null
     */
    protected $department_id;

    /**
     * employee manager id
     * @var integer|null
     */
    protected $manager_id;

    /**
     * employee coach id
     * @var integer|null
     */
    protected $coach_id;

    /**
     * employee picture
     * @var string|null
     */
    protected $pic;

    /**
     * employee work phone
     * @var string|null
     */
    protected $work_phone;
    /**
     * employee work phone
     * @var string|null
     */
    protected $work_mobile;

    /**
     * employee work email
     * @var string|null
     */
    protected $work_email;

    /**
     * employee work location id
     * @var integer|null
     */
    protected $work_location_id;

    /**
     * employee expense approved by employee
     * @var integer|null
     */
    protected $approvers_expense;

    /**
     * employee time off approved by employee
     * @var integer|null
     */
    protected $approvers_timeoff;

    /**
     * ID shift for working hour
     * @var integer|null
     */
    protected $working_hours_id;

    /**
     * ID timezone for working
     * @var integer|null
     */
    protected $timezone;

    /**
     * user id for employess to login to a portal
     * @var string|null
     */
    protected $related_user_id;

    /**
     * Id for current contract employee
     * @var integer|null
     */
    protected $current_contract_id;

    /**
     * pin code for employee
     * @var integer|null
     */
    protected $pin_code;

    /**
     * badge ID for employee to use on the card employee
     * @var integer|null
     */
    protected $badge_id;

    /**
     * tenant id
     * @var string
     */
    protected $tenant_id;

    /**
     * company id
     * @var string
     */
    protected $company_id;

    /**
     * branch id
     * @var string
     */
    protected $branch_id;

    /**
     * created by user id
     * @var string
     */
    protected $created_by;

    /**
     * created at
     * @var string
     */
    protected $created_at;

    /**
     * updated at
     * @var string
     */
    protected $updated_at;


    public function __construct() {

    }

    public function setNewEmployee($newEmployee) {
        $this->setEmpNo($newEmployee['emp_no']);
        $this->setNameEn($newEmployee['name_en']);
        $this->setNameAr($newEmployee['name_ar']);
        $this->setJobPositionId($newEmployee['job_position_id']);
        $this->setEmployeeTypeId($newEmployee['employee_type_id']);
        $this->setTags($newEmployee['tags']);
        $this->setDepartmentId($newEmployee['department_id']);
        $this->setManagerId($newEmployee['manager_id']);
        $this->setCoachId($newEmployee['coach_id']);
        $this->setPic($newEmployee['pic']);
        $this->setWorkPhone($newEmployee['work_phone']);
        $this->setWorkMobile($newEmployee['work_mobile']);
        $this->setWorkEmail($newEmployee['work_email']);
        $this->setWorkLocationId($newEmployee['work_location_id']);
        $this->setApproversExpense($newEmployee['approvers_expense']);
        $this->setApproversTimeOff($newEmployee['approvers_timeoff']);
        $this->setWorkingHoursId($newEmployee['working_hours_id']);
        $this->setTimezone($newEmployee['timezone']);
        $this->setRelatedUserId($newEmployee['related_user_id']);
        $this->setCurrentContractId($newEmployee['current_contract_id']);
        $this->setPinCode($newEmployee['pin_code']);
        $this->setBadgeId($newEmployee['badge_id']);
        $this->setTenantId($newEmployee['tenant_id']);
        $this->setCompanyId($newEmployee['company_id']);
        $this->setBranchId($newEmployee['branch_id']);
        $this->setCreatedBy($newEmployee['created_by']);
    }

    public function getEmployee() {
        return [
            'emp_no'  => $this->getEmpNo(),
            'name_en' => $this->getNameEn(),
            'name_ar' => $this->getNameAr(),
            'job_position_id' => $this->getJobPositionId(),
            'employee_type_id' => $this->getEmployeeTypeId(),
            'tags' => $this->getTags(),
            'department_id' => $this->getDepartmentId(),
            'manager_id' => $this->getManagerId(),
            'coach_id' => $this->getCoachId(),
            'pic' => $this->getPic(),
            'work_phone' => $this->getWorkPhone(),
            'work_mobile' => $this->getWorkMobile(),
            'work_email' => $this->getWorkEmail(),
            'work_location_id' => $this->getWorkLocationId(),
            'approvers_expense' => $this->getApproversExpense(),
            'approvers_timeoff' => $this->getApproversTimeOff(),
            'working_hours_id' => $this->getWorkingHoursId(),
            'timezone' => $this->getTimezone(),
            'related_user_id' => $this->getRelatedUserId(),
            'current_contract_id' => $this->getCurrentContractId(),
            'pin_code' => $this->getPinCode(),
            'badge_id' => $this->getBadgeId(),
            'tenant_id' => $this->getTenantId(),
            'company_id' => $this->getCompanyId(),
            'branch_id' => $this->getBranchId(),
            'created_by' => $this->getCreatedBy()
        ];
    }



    /**
     * Set the employee Number.
     * @param  string  $value
     * @return $this
     */
    public function setEmpNo($value) {
        $this->emp_no = $value;
        return $this;
    }

    public function getEmpNo() {
        return $this->emp_no ;
    }

    /**
     * Set the employee english name.
     * @param  string  $value
     * @return $this
     */
    public function setNameEn($value) {
        $this->name_en = $value;
        return $this;
    }

    public function getNameEn() {
        return $this->name_en ;
    }

    /**
     * Set the employee arabic name.
     * @param  string|null   $value
     * @return $this
     */
    public function setNameAr($value) {
        $this->name_ar = $value;
        return $this;
    }

    public function getNameAr() {
        return $this->name_ar ;
    }

    /**
     * Set the employee job postion.
     * @param  integer|null   $value
     * @return $this
     */
    public function setJobPositionId($value) {
        $this->job_position_id = $value;
        return $this;
    }
    public function getJobPositionId() {
        return $this->job_position_id ;
    }

    /**
     * Set the employee type ID.
     * @param  integer|null   $value
     * @return $this
     */
    public function setEmployeeTypeId($value) {
        $this->employee_type_id = $value;
        return $this;
    }

    public function getEmployeeTypeId() {
        return $this->employee_type_id ;
    }

    /**
     * Set the employee Tags.
     * @param  array|null   $value
     * @return $this
     */
    public function setTags($value) {
        $this->tags = $value;
        return $this;
    }
    public function getTags() {
        return $this->tags ;
    }

    /**
     * Set the employee Department ID.
     * @param  integer|null   $value
     * @return $this
     */
    public function setDepartmentId($value) {
        $this->department_id = $value;
        return $this;
    }
    public function getDepartmentId() {
        return $this->department_id ;
    }

    /**
     * Set the employee Manager ID.
     * @param  integer|null   $value
     * @return $this
     */
    public function setManagerId($value) {
        $this->manager_id = $value;
        return $this;
    }

    public function getManagerId() {
        return $this->manager_id ;
    }

    /**
     * Set the employee Coach ID.
     * @param  integer|null  $value
     * @return $this
     */
    public function setCoachId($value) {
        $this->coach_id = $value;
        return $this;
    }
    public function getCoachId() {
        return $this->coach_id ;
    }

    /**
     * Set the employee Picture.
     * @param  string|null  $value
     * @return $this
     */
    public function setPic($value) {
        $this->pic = $value;
        return $this;
    }
    public function getPic() {
        return $this->pic ;
    }

    /**
     * Set the employee Work phone.
     * @param  string|null  $value
     * @return $this
     */
    public function setWorkPhone($value) {
        $this->work_phone = $value;
        return $this;
    }
    public function getWorkPhone() {
        return $this->work_phone ;
    }

    /**
     * Set the employee Work mobile.
     * @param  string|null  $value
     * @return $this
     */
    public function setWorkMobile($value) {
        $this->work_mobile = $value;
        return $this;
    }
    public function getWorkMobile() {
        return $this->work_mobile ;
    }

    /**
     * Set the employee Work email.
     * @param  string|null  $value
     * @return $this
     */
    public function setWorkEmail($value) {
        $this->work_email = $value;
        return $this;
    }

    public function getWorkEmail() {
        return $this->work_email ;
    }

    /**
     * Set the employee Work location id.
     * @param  integer|null  $value
     * @return $this
     */
    public function setWorkLocationId($value) {
        $this->work_location_id = $value;
        return $this;
    }

    public function getWorkLocationId() {
        return $this->work_location_id ;
    }

    /**
     * Set the employee response about approvers expense .
     * @param  integer|null  $value
     * @return $this
     */
    public function setApproversExpense($value) {
        $this->approvers_expense = $value;
        return $this;
    }

    public function getApproversExpense() {
        return $this->approvers_expense ;
    }

    /**
     * Set the employee response about approvers time off .
     * @param  integer|null  $value
     * @return $this
     */
    public function setApproversTimeOff($value) {
        $this->approvers_timeoff = $value;
        return $this;
    }

    public function getApproversTimeOff() {
        return $this->approvers_timeoff;
    }

    /**
     * Set the shift about working hours id .
     * @param  integer|null  $value
     * @return $this
     */
    public function setWorkingHoursId($value) {
        $this->working_hours_id = $value;
        return $this;
    }

    public function getWorkingHoursId() {
        return $this->working_hours_id;
    }

    /**
     * Set the timezone id .
     * @param  integer|null  $value
     * @return $this
     */
    public function setTimezone($value) {
        $this->timezone = $value;
        return $this;
    }
    public function getTimezone() {
        return $this->timezone ;
    }

    /**
     * Set the related user .
     * @param  string|null  $value
     * @return $this
     */
    public function setRelatedUserId($value) {
        $this->related_user_id = $value;
        return $this;
    }
    public function getRelatedUserId() {
        return $this->related_user_id ;
    }

    /**
     * Set the current contract is using .
     * @param  integer|null  $value
     * @return $this
     */
    public function setCurrentContractId($value) {
        $this->current_contract_id = $value;
        return $this;
    }
    public function getCurrentContractId() {
        return $this->current_contract_id ;
    }

    /**
     * Set the pin code for user .
     * @param  integer|null  $value
     * @return $this
     */
    public function setPinCode($value) {
        $this->pin_code = $value;
        return $this;
    }
    public function getPinCode() {
        return $this->pin_code ;
    }

    /**
     * Set the pin code for user .
     * @param  integer|null  $value
     * @return $this
     */
    public function setBadgeId($value) {
        $this->badge_id = $value;
        return $this;
    }
    public function getBadgeId() {
        return $this->badge_id ;
    }

    /**
     * Set the Tenant ID .
     * @param  string  $value
     * @return $this
     */
    public function setTenantId($value) {
        $this->tenant_id = $value;
        return $this;
    }
    public function getTenantId() {
        return $this->tenant_id ;
    }

    /**
     * Set the company ID .
     * @param  string  $value
     * @return $this
     */
    public function setCompanyId($value) {
        $this->company_id = $value;
        return $this;
    }
    public function getCompanyId() {
        return $this->company_id ;
    }

    /**
     * Set the branch ID .
     * @param  string  $value
     * @return $this
     */
    public function setBranchId($value) {
        $this->branch_id = $value;
        return $this;
    }
    public function getBranchId() {
        return $this->branch_id ;
    }

    /**
     * Set the branch ID .
     * @param  string  $value
     * @return $this
     */
    public function setCreatedBy($value) {
        $this->created_by = $value;
        return $this;
    }
    public function getCreatedBy() {
        return $this->created_by ;
    }






}
