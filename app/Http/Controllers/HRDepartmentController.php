<?php

namespace App\Http\Controllers;

use App\Services\HrDepartmentService;
use Illuminate\Http\Request;

class HRDepartmentController extends Controller
{
    public function store(Request $request,HrDepartmentService $hrDepartmentService) {
        if($request->input('name')){
            $result = $hrDepartmentService->addNew($request->all());
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "successfully added",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during adding ",
        ],401);
    }
    public function update($id,Request $request,HrDepartmentService $hrDepartmentService) {
        if($request->input('name')){
            $result = $hrDepartmentService->updateRow($id,$request->all());
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => " updated successfully",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during adding ",
        ],401);
    }

    public function getDepartments(Request $request,HrDepartmentService $hrDepartmentService) {
        $tenant_id  = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id  = $request->input('branch_id');

        if($tenant_id && $company_id && $branch_id){
            $result = $hrDepartmentService->getByTenant($tenant_id,$company_id,$branch_id);
            if($result){

                return response()->json([
                    "status" => 1,
                    "message" => "successfully fetched",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }

    public function delete($id,HrDepartmentService $hrDepartmentService){
        $result = $hrDepartmentService->delete($id);
        if($result){

            return response()->json([
                "status" => 1,
                "message" => "successfully deleted",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during deleting data ",
        ],401);
    }



}
