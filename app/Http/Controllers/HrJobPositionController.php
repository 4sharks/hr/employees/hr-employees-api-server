<?php

namespace App\Http\Controllers;

use App\Services\HrJobPositionService;
use Illuminate\Http\Request;

class HrJobPositionController extends Controller
{
    private $hr_job_position_service;

    public function __construct()
    {
        $this->hr_job_position_service = new HrJobPositionService();
    }

    public function store(Request $request) {
        if($request->input('name')){
            $result = $this->hr_job_position_service->addNew($request->all());
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "successfully added job position",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during adding data ",
        ],401);
    }

    public function update($id,Request $request) {
        if($request->input('name')){
            $result = $this->hr_job_position_service->updateRow($id,$request->all());
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "successfully updated job position",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during update data ",
        ],401);
    }

    public function getJobPositions(Request $request) {
        if($request->input('tenant_id') && $request->input('company_id') && $request->input('branch_id')){
            $result = $this->hr_job_position_service->getByTenant($request->all());
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "successfully fetched job position",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }

    public function delete($id){
        $result = $this->hr_job_position_service->delete($id);
        if($result){

            return response()->json([
                "status" => 1,
                "message" => "successfully deleted",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during deleting data ",
        ],401);
    }
}
