<?php

namespace App\Http\Controllers;

use App\Services\HrEmployeeService;
use Illuminate\Http\Request;

class HREmployeesController extends Controller
{
    private $hrEmployeeService;
    private $tenant_id;
    private $company_id;
    private $branch_id ;

    public function __construct()
    {
        $this->hrEmployeeService = new HREmployeeService();
    }

    public function store(Request $request) {
        $result = $this->hrEmployeeService->addNew($request);
        if($result['status'] === 1 || $result['status'] === 0){
            return response()->json($result,201);
        }
        return response()->json($result,401);
    }

    public function update($id,Request $request) {
        $result = $this->hrEmployeeService->update($id,$request);
        if($result['status'] === 1 || $result['status'] === 0){
            return response()->json($result,201);
        }
        return response()->json($result,401);
    }

    public function getEmployees(Request $request) {
        $this->tenant_id  = $request->input('tenant_id');
        $this->company_id = $request->input('company_id');
        $this->branch_id  = $request->input('branch_id');
        $result = $this->hrEmployeeService->getByTenant($this->tenant_id,$this->company_id,$this->branch_id);

        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully fetched",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }

    public function getEmployeeById($tenantId,$companyId,$branchId,$id) {

        $result = $this->hrEmployeeService->getById($tenantId,$companyId,$branchId,$id);

        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully fetched",
                "data"=> $result
            ],200);
        }

        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);

    }

    public function getEmployeeByUserId($userId) {

        $result = $this->hrEmployeeService->getByUserId($userId);

        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully fetched",
                "data"=> $result
            ],200);
        }

        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
            "data"=> $result
        ],401);

    }

    public function getEmployeesByDept(Request $request) {
        $this->tenant_id  = $request->input('tenant_id');
        $this->company_id = $request->input('company_id');
        $this->branch_id  = $request->input('branch_id');
        $department_id  = $request->input('department_id');
        $result = $this->hrEmployeeService->getByDept($this->tenant_id,$this->company_id,$this->branch_id,$department_id);

        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully fetched",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }

    public function delete($id){
        $result = $this->hrEmployeeService->delete($id);
        if($result){

            return response()->json([
                "status" => 1,
                "message" => "successfully deleted",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during deleting data ",
        ],401);
    }

}
