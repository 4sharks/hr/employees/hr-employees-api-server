<?php

namespace App\Http\Controllers;

use App\Models\HrSkillsLevelsModel;
use App\Models\HrSkillsModel;
use App\Models\HrSkillsType;
use Illuminate\Http\Request;

class HrSkillsTypesController extends Controller
{

    public function index($tenant_id,$company_id){
        if( !$tenant_id || !$company_id){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsType::with(['skills:id,title,skill_type_id','levels:id,title,level_percent,skill_type_id'])->where([
            'tenant_id' => $tenant_id,
            'company_id' => $company_id,
        ])->get();

        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully",
                "data" => $result
            ],201);
        }

        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);

    }

    public function storeAll(Request $request){
        if(!$request->input('name') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }

        $type = HrSkillsType::create([
            'name' => $request->input('name'),
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'branch_id' => $request->input('branch_id'),
            'created_by' => $request->input('created_by'),
        ]);

        if($type){
            $skills = [];
            foreach($request->input('skills') as $skill){
                $skills[] = HrSkillsModel::create([
                    'skill_type_id' => $type->id,
                    'title' => $skill,
                    'tenant_id' => $request->input('tenant_id'),
                    'company_id' => $request->input('company_id'),
                    'branch_id' => $request->input('branch_id'),
                    'created_by' => $request->input('created_by'),
                ]);
            }
            $levels = array();
            foreach($request->input('levels') as $level){
                $levels[] = HrSkillsLevelsModel::create([
                    'skill_type_id' => $type->id,
                    'title' => $level['title'],
                    'level_percent' => $level['progress'],
                    'tenant_id' => $request->input('tenant_id'),
                    'company_id' => $request->input('company_id'),
                    'branch_id' => $request->input('branch_id'),
                    'created_by' => $request->input('created_by'),
                ]);
            }
            return response()->json([
                "status" => 1,
                "message" => "Successfully added ",
                "data" => [
                    "type" => $type,
                    "skills" => $skills,
                    "level" => $levels
                ]
            ],200);
        }

        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);

    }

    public function store(Request $request){
        if(!$request->input('name') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsType::create([
            'name' => $request->input('name'),
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'branch_id' => $request->input('branch_id'),
            'created_by' => $request->input('created_by'),
        ]);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully added ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);
    }

    public function update(Request $request,$id){
        if(!$request->input('name') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsType::find($id);
        if($result){
            $updated = $result->update([
                'name' => $request->input('name'),
                'tenant_id' => $request->input('tenant_id'),
                'company_id' => $request->input('company_id'),
                'branch_id' => $request->input('branch_id'),
                'created_by' => $request->input('created_by'),
            ]);
            if($updated){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully updated ",
                    "data" => $updated
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);
    }

    public function show($id){
        $result = HrSkillsType::find($id);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function showAll($tenant_id,$company_id) {
        $result = HrSkillsType::where([
            'tenant_id' => $tenant_id,
            'company_id' => $company_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function delete($id){
        HrSkillsLevelsModel::where([
            "skill_type_id" => $id,
        ])->delete();
        HrSkillsModel::where([
            "skill_type_id" => $id,
        ])->delete();

        $result = HrSkillsType::find($id)->delete();

        if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully",
                    "data" => $result
                ],200);

        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

}
