<?php

namespace App\Http\Controllers;

use App\Models\HrSkillsLevelsModel;
use Illuminate\Http\Request;

class HrSkillsLevelsController extends Controller
{

    public function store(Request $request){
        if(!$request->input('title') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsLevelsModel::create([
            'skill_type_id' => $request->input('skill_type_id'),
            'title' => $request->input('title'),
            'level_percent' => $request->input('level_percent'),
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'branch_id' => $request->input('branch_id'),
            'created_by' => $request->input('created_by'),
        ]);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully added ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);
    }

    public function update(Request $request,$id){
        if(!$id || !$request->input('title') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsLevelsModel::find($id);
        if($result){
            $updated = $result->update([
                'skill_type_id' => $request->input('skill_type_id'),
                'title' => $request->input('title'),
                'level_percent' => $request->input('level_percent'),
                'tenant_id' => $request->input('tenant_id'),
                'company_id' => $request->input('company_id'),
                'branch_id' => $request->input('branch_id'),
                'created_by' => $request->input('created_by'),
            ]);
            if($updated){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully updated ",
                    "data" => $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid update request",
        ],401);
    }

    public function show($id){
        $result = HrSkillsLevelsModel::find($id);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function showAll($tenant_id,$company_id) {
        $result = HrSkillsLevelsModel::where([
            'tenant_id' => $tenant_id,
            'company_id' => $company_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }
    public function showByTypeId($tenant_id,$company_id,$type_id) {
        $result = HrSkillsLevelsModel::where([
            'skill_type_id' => $type_id,
            'tenant_id' => $tenant_id,
            'company_id' => $company_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function delete($id){
        $result = HrSkillsLevelsModel::find($id);
        if($result){
            $delete = $result->delete();
            if($delete){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully",
                    "data" => $delete
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }
}
