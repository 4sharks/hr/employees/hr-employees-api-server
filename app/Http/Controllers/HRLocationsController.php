<?php

namespace App\Http\Controllers;

use App\Services\HrLocationService;
use Illuminate\Http\Request;

class HRLocationsController extends Controller
{
    public $locationService;

    public function __construct()
    {
        $this->locationService = new HrLocationService();
    }

    public function store(Request $request){
        if($request->input('address_country_id') && $request->input('location_name') ){
            $added = $this->locationService->addNew($request->all());
            if($added){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully added location",
                    "data" => $added
                ],200);
            }
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Invalid",
            ],401);
        }
    }
    public function update($id,Request $request){
        if( $request->input('location_name') ){
            $updated = $this->locationService->updateLocation($id,$request->all());
            if($updated){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully updated location",
                    "data" => $updated
                ],200);
            }
        }else{
            return response()->json([
                "status" => 0,
                "message" => "Invalid update",
            ],401);
        }
    }

    public function getLocations(Request $request) {

        $tenant_id  = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id  = $request->input('branch_id');

        if($tenant_id && $company_id && $branch_id){
            $result = $this->locationService->getByTenant($tenant_id,$company_id,$branch_id);
            if($result){
                return response()->json([
                    "status" => 1,
                    "message" => "successfully fetched job position",
                    "data"=> $result
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }


    public function delete($id){
        $result = $this->locationService->delete($id);
        if($result){

            return response()->json([
                "status" => 1,
                "message" => "successfully deleted",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during deleting data ",
        ],401);
    }

}
