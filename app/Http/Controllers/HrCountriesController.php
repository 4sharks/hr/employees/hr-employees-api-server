<?php

namespace App\Http\Controllers;

use App\Services\HrCountryService;
use Illuminate\Http\Request;

class HrCountriesController extends Controller
{
    public $countryService;

    public function __construct()
    {
        $this->countryService = new HrCountryService();
    }

    public function getAll()
    {
        $result = $this->countryService->getAll();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully Fetched all countries",
                "data"=> $result
            ],200);
        }
    }


}
