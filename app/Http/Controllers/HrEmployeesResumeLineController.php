<?php

namespace App\Http\Controllers;

use App\Services\HrEmployeesResumeLineService;
use Illuminate\Http\Request;

class HrEmployeesResumeLineController extends Controller
{
    protected $resumeLineService;

    public function __construct()
    {
        $this->resumeLineService = new HrEmployeesResumeLineService();
    }

    public function store(Request $request){
        $emp_id = $request->input('emp_id');
        $title  = $request->input('title');
        $type   = $request->input('type');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $description = $request->input('description');
        $created_by = $request->input('created_by');
        $tenant_id     = $request->input('tenant_id');
        $company_id    = $request->input('company_id');

        if($emp_id && $title){
            $result = $this->resumeLineService->addNew([
                "emp_id" => $emp_id,
                "title" => $title,
                "type"  => $type,
                "date_start" => $date_start,
                "date_end" => $date_end,
                "description" => $description,
                "created_by" => $created_by,
                "tenant_id" => $tenant_id,
                "company_id" => $company_id,
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }else{
            return response()->json([
                "status" => 0 ,
                "message" => 'All fields are required',
            ],404);
        }

        return response()->json([
            "status" => 0 ,
            "message" => 'Error adding '
        ],404);

    }

    public function update($id,Request $request){
        $emp_id = $request->input('emp_id');
        $title  = $request->input('title');
        $type   = $request->input('type');
        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $description = $request->input('description');
        $created_by = $request->input('created_by');
        $tenant_id     = $request->input('tenant_id');
        $company_id    = $request->input('company_id');

        if($emp_id && $title){
            $result = $this->resumeLineService->update($id,[
                "emp_id" => $emp_id,
                "title" => $title,
                "type"  => $type,
                "date_start" => $date_start,
                "date_end" => $date_end,
                "description" => $description,
                "created_by" => $created_by,
                "tenant_id" => $tenant_id,
                "company_id" => $company_id,
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }else{
            return response()->json([
                "status" => 0 ,
                "message" => 'All fields are required',
            ],404);
        }

        return response()->json([
            "status" => 0 ,
            "message" => 'Error adding '
        ],404);

    }


    public function showAll($tenant_id,$company_id){
        if($tenant_id && $company_id){
            $result = $this->resumeLineService->getBy([
                "tenant_id" => $tenant_id,
                "company_id" => $company_id,
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error fetching '
        ],404);
    }

    public function show($id){
        if($id){
            $result = $this->resumeLineService->getById($id);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error fetching '
        ],404);
    }

    public function delete($id){
        if($id){
            $result = $this->resumeLineService->deleteById($id);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error fetching '
        ],404);
    }




}
