<?php

namespace App\Http\Controllers;

use App\Services\HrEmployeeTypesService;
use Illuminate\Http\Request;

class HrEmployeeTypesController extends Controller
{
    public $employeeTypesService;

    public function __construct()
    {
        $this->employeeTypesService = new HrEmployeeTypesService();
    }

    public function getEmployeeTypes(){
        $result = $this->employeeTypesService->getAll();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "successfully fetched",
                "data"=> $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Error during fetching data ",
        ],401);
    }


}
