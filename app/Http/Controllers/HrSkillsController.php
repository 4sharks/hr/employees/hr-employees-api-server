<?php

namespace App\Http\Controllers;

use App\Models\HrSkillsModel;
use Illuminate\Http\Request;

class HrSkillsController extends Controller
{

    public function store(Request $request){
        if( !$request->input('skill_type_id') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsModel::create([
            'skill_type_id' => $request->input('skill_type_id'),
            'title' => $request->input('title'),
            'tenant_id' => $request->input('tenant_id'),
            'company_id' => $request->input('company_id'),
            'branch_id' => $request->input('branch_id'),
            'created_by' => $request->input('created_by'),
        ]);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully added ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);
    }

    public function update(Request $request,$id){
        if(!$request->input('skill_type_id') || !$request->input('tenant_id') || !$request->input('company_id')){
            return response()->json([
                "status" => 0,
                "message" => "All fields are required",
            ],401);
        }
        $result = HrSkillsModel::find($id);
        if($result){
            $updated = $result->update([
                'skill_type_id' => $request->input('skill_type_id'),
                'title' => $request->input('title'),
                'tenant_id' => $request->input('tenant_id'),
                'company_id' => $request->input('company_id'),
                'branch_id' => $request->input('branch_id'),
                'created_by' => $request->input('created_by'),
            ]);
            if($updated){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully updated ",
                    "data" => $updated
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid",
        ],401);
    }

    public function show($id){
        $result = HrSkillsModel::find($id);
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function showAll($tenant_id,$company_id) {
        $result = HrSkillsModel::where([
            'tenant_id' => $tenant_id,
            'company_id' => $company_id
        ])->get();
        if($result){
            return response()->json([
                "status" => 1,
                "message" => "Successfully ",
                "data" => $result
            ],200);
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }

    public function delete($id){
        $result = HrSkillsModel::find($id);
        if($result){
            $delete = $result->delete();
            if($delete){
                return response()->json([
                    "status" => 1,
                    "message" => "Successfully",
                    "data" => $delete
                ],200);
            }
        }
        return response()->json([
            "status" => 0,
            "message" => "Invalid id ",
        ],401);
    }
}
