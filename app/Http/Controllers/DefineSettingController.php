<?php

namespace App\Http\Controllers;

use App\Services\DefineSettingService;
use Illuminate\Http\Request;

class DefineSettingController extends Controller
{
    protected $defineSettingService;

    public function __construct()
    {
        $this->defineSettingService = new DefineSettingService();
    }

    public function store(Request $request){

        $setting_name  = $request->input('setting_name');
        $setting_class = $request->input('setting_class');
        $setting_label = $request->input('label');
        $setting_value = $request->input('value');
        $setting_lang  = $request->input('lang');
        $tenant_id     = $request->input('tenant_id');
        $company_id    = $request->input('company_id');
        $created_by    = $request->input('created_by');

        if($setting_name && $setting_label && $setting_value){

            $result = $this->defineSettingService->addNew([
                "setting_name"  => $setting_name,
                "setting_class" => $setting_class,
                "label" => $setting_label,
                "value" => $setting_value,
                "lang"  => $setting_lang,
                "tenant_id"     => $tenant_id,
                "company_id"    => $company_id,
                "created_by" => $created_by,
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }else{
            return response()->json([
                "status" => 0 ,
                "message" => 'All fields are required',
            ],404);
        }

        return response()->json([
            "status" => 0 ,
            "message" => 'Error adding setting'
        ],404);

    }

    public function update($id,Request $request){

        $setting_name  = $request->input('setting_name');
        $setting_class = $request->input('setting_class');
        $setting_label = $request->input('label');
        $setting_value = $request->input('value');
        $setting_lang  = $request->input('lang');
        $tenant_id     = $request->input('tenant_id');
        $company_id    = $request->input('company_id');
        $created_by    = $request->input('created_by');

        if($setting_name && $setting_label && $setting_value){

            $result = $this->defineSettingService->update($id,[
                "setting_name"  => $setting_name,
                "setting_class" => $setting_class,
                "label" => $setting_label,
                "value" => $setting_value,
                "lang"  => $setting_lang,
                "tenant_id"     => $tenant_id,
                "company_id"    => $company_id,
                "created_by" => $created_by,
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }else{
            return response()->json([
                "status" => 0 ,
                "message" => 'All fields are required',
            ],404);
        }

        return response()->json([
            "status" => 0 ,
            "message" => 'Error updating setting'
        ],404);

    }

    public function showAll($tenant_id,$company_id,$setting_name){
        if($tenant_id && $company_id && $setting_name){
            $result = $this->defineSettingService->getBy([
                "tenant_id" => $tenant_id,
                "company_id" => $company_id,
                "setting_name" => $setting_name
            ]);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'Successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error fetching setting'
        ],404);
    }

    public function show($id){
        if($id){
            $result = $this->defineSettingService->getById($id);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error fetching setting'
        ],404);
    }

    public function delete($id){
        if($id){
            $result = $this->defineSettingService->deleteById($id);
            if($result){
                return response()->json([
                    "status" => 1 ,
                    "message" => 'successfully',
                    "data" => $result
                ],201);
            }
        }
        return response()->json([
            "status" => 0 ,
            "message" => 'Error during deletion'
        ],404);
    }
}
