<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeesResumeLine extends Model
{
    use HasFactory;

    protected $table = 'hr_employees_resume_lines';

    protected $guarded = [];
}
