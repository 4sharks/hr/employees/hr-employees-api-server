<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrAddressModel extends Model
{
    use HasFactory;

    protected $table = 'hr_addresses';

    protected $guarded = [];

    public function countryRelation(){
        return $this->hasOne(HrCountryModel::class,'id','country_id');
    }

}
