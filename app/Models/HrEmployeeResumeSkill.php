<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class HrEmployeeResumeSkill extends Model
{
    use HasFactory;

    protected $table = 'hr_employee_resume_skills';

    protected $guarded = [];

    public function type():HasOne {
        return $this->hasOne(HrSkillsType::class,'id','skill_type_id');
    }

    public function skills():HasOne {
        return $this->hasOne(HrSkillsModel::class,'id','skill_id');
    }

    public function levels():HasOne {
        return $this->hasOne(HrSkillsLevelsModel::class,'id','skill_level_id');
    }

}
