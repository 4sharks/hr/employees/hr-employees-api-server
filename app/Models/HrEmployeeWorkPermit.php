<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeeWorkPermit extends Model
{
    use HasFactory;

    protected $table = 'hr_employees_work_permits';

    protected $guarded = [];
}
