<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeeModel extends Model
{
    use HasFactory;

    protected $table = 'hr_employees';

    protected $guarded = [];

    public function departmentRelation(){
        return $this->hasOne(HrDepartmentModel::class,'id','department_id');
    }

    public function job_position(){
        return $this->hasOne(HrJobPositionModel::class,'id','job_position_id');
    }

    public function manager(){
        return $this->hasOne(HrEmployeeModel::class,'id','manager_id');
    }

    public function coach(){
        return $this->hasOne(HrEmployeeModel::class,'id','coach_id');
    }

    public function approversExpense(){
        return $this->hasOne(HrEmployeeModel::class,'id','approvers_expense');
    }
    public function approversTimeoff(){
        return $this->hasOne(HrEmployeeModel::class,'id','approvers_timeoff');
    }

    public function work_location(){
        return $this->hasOne(HrLocationModel::class,'id','work_location_id');
    }

    public function working_hours(){
        return $this->hasOne(DefineSettingModel::class,'id','working_hours_id');
    }

    public function timeZone(){
        return $this->hasOne(DefineSettingModel::class,'id','timezone');
    }

    public function private_info(){
        return $this->hasOne(HrEmployeePrivateInfoModel::class,'emp_id','id');
    }

    public function work_permit(){
        return $this->hasOne(HrEmployeeWorkPermit::class,'emp_id','id');
    }

    public function config(){
        return $this->hasOne(HrEmployeeConfigModel::class,'emp_id','id');
    }

    public function resume_lines(){
        return $this->hasMany(HrEmployeesResumeLine::class,'emp_id','id');
    }

    public function resume_skills(){
        return $this->hasMany(HrEmployeeResumeSkill::class,'emp_id','id');
    }




}
