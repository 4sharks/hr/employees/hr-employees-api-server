<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefineSettingModel extends Model
{
    use HasFactory;

    protected $table = 'define_settings';

    protected $guarded = [];
}
