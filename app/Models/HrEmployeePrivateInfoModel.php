<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeePrivateInfoModel extends Model
{
    use HasFactory;

    protected $table = 'hr_employees_private_info';
    protected $guarded = [];

    public function nationality(){
        return $this->hasOne(HrCountryModel::class,'id','nationality_id');
    }
}
