<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrLocationModel extends Model
{
    use HasFactory;

    protected $table = 'hr_locations';

    protected $guarded = [];

    public function addressRelation(){
        return $this->hasOne(HrAddressModel::class,'id','address_id');
    }
}
