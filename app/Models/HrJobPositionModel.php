<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrJobPositionModel extends Model
{
    use HasFactory;

    protected $table = 'hr_job_position';

    protected $guarded = [];

    public function recruiter(){
        return $this->hasOne(HrEmployeeModel::class,'id','recruiter_id');
    }
    public function location(){
        return $this->hasOne(HrLocationModel::class,'id','location_id');
    }

    public function department(){
        return $this->hasOne(HrDepartmentModel::class,'id','department_id')->select(['id','name','parent_id']);
    }

    public function employee_types(){
        return $this->hasOne(HrEmployeeTypeModel::class,'id','employee_type_id');
    }

}
