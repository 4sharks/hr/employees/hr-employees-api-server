<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrSkillsLevelsModel extends Model
{
    use HasFactory;

    protected $table = 'hr_skills_level';

    protected $guarded = [];

}
