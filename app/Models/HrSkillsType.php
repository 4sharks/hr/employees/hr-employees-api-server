<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrSkillsType extends Model
{
    use HasFactory;

    protected $table = "hr_skills_type";

    protected $guarded = [];

    public function skills(){
        return $this->hasMany(HrSkillsModel::class,'skill_type_id','id');
    }

    public function levels(){
        return $this->hasMany(HrSkillsLevelsModel::class,'skill_type_id','id');
    }

}
