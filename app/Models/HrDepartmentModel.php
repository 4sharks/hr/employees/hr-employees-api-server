<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrDepartmentModel extends Model
{
    use HasFactory;

    protected $table = 'hr_departments';

    protected $guarded = [];

    protected $appends = [
        'employees_count',
    ];

    public function parentRelation(){
        return $this->hasOne(HrDepartmentModel::class,'id','parent_id')->select(['id','name','parent_id']);
    }

    public function managerRelation(){
        return $this->hasOne(HrEmployeeModel::class,'id','manager_id');
    }

    public function getEmployeesCountAttribute(){
        return HrEmployeeModel::where([
            'department_id' => $this->id
        ])->count();
    }

}

