<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeeConfigModel extends Model
{
    use HasFactory;

    protected $table = 'hr_employees_config';

    protected $guarded = [];

}
