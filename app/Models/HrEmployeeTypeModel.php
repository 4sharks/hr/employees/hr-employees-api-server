<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrEmployeeTypeModel extends Model
{
    use HasFactory;

    protected $table = 'hr_employee_types';

    protected $guarded = [];
}
