<?php
namespace App\Services;

use App\Models\DefineSettingModel;

class DefineSettingService extends Service{

    public function addNew($data)
    {
        $result = DefineSettingModel::create($data);
        if($result){
            return $result;
        }
        return false;
    }

    public function update($id,$data)
    {
        $result = DefineSettingModel::find($id);
        if($result){
            $updated = $result->update($data);
            if($updated){
                return $updated;
            }
            return false;
        }
        return false;
    }

    public function getBy($where = [])
    {
        $result = DefineSettingModel::where($where)->orWhere([
            'tenant_id' => null,
            'company_id' => null,
        ])->orderBy('id','desc')->get();
        if($result){
            return $result;
        }
        return false;
    }

    public function getById($id)
    {
        $result = DefineSettingModel::find($id);
        if($result){
            return $result;
        }
        return false;
    }

    public function deleteBy($where = [])
    {
        $result = DefineSettingModel::where($where)->get();
        if($result){
            $deleted = $result->delete();
            return $deleted;
        }
        return false;
    }

    public function deleteById($id)
    {
        $result = DefineSettingModel::find($id);
        if($result){
            $deleted = $result->delete();
            return $deleted;
        }
        return false;
    }



}
