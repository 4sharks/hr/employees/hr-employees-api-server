<?php
namespace App\Services;

class Service extends BaseService {

    protected $className = null;

    public function addNew($data)
    {
        $result = $this->className::create($data);
        if($result){
            return $result;
        }
        return false;
    }

    public function update($id,$data)
    {
        $result = $this->className::find($id);
        if($result){
            $updated = $result->update($data);
            if($updated){
                return $updated;
            }
            return false;
        }
        return false;
    }

    public function getBy($where = [])
    {
        $result = $this->className::where($where)->orderBy('id','desc')->get();
        if($result){
            return $result;
        }
        return false;
    }

    public function getById($id)
    {
        $result = $this->className::find($id);
        if($result){
            return $result;
        }
        return false;
    }

    public function deleteBy($where = [])
    {
        $result = $this->className::where($where)->get();
        if($result){
            $deleted = $result->delete();
            return $deleted;
        }
        return false;
    }

    public function deleteById($id)
    {
        $result = $this->className::find($id);
        if($result){
            $deleted = $result->delete();
            return $deleted;
        }
        return false;
    }

    // public function addNew($data){

    // }


    // public function getBy($where=[]){

    // }

    // public function getById($id){

    // }

    // public function deleteBy($where=[]){

    // }
    // public function deleteById($id){

    // }
}
