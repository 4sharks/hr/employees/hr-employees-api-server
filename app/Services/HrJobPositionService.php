<?php
namespace App\Services;

use App\Models\HrJobPositionModel;

class HrJobPositionService {

    public function addNew($_data){

        $jobPosition['name'] = $_data['name'];
        $jobPosition['department_id'] = $_data['department_id'];
        $jobPosition['location_id'] = $_data['location_id'];
        $jobPosition['employee_type_id'] = $_data['employee_type_id'];
        $jobPosition['is_published'] = $_data['is_published'];
        $jobPosition['target'] = $_data['target'];
        $jobPosition['recruiter_id'] = $_data['recruiter_id'];
        $jobPosition['intervieews_ids'] = $_data['intervieews_ids'];
        $jobPosition['job_summary'] = $_data['job_summary'];
        $jobPosition['tenant_id'] = $_data['tenant_id'];
        $jobPosition['company_id'] = $_data['company_id'];
        $jobPosition['branch_id'] = $_data['branch_id'];
        $jobPosition['created_by'] = $_data['created_by'];

        $result = HrJobPositionModel::create($jobPosition);
        if($result)
            return $result;
        else
            return false;

    }
    public function updateRow($id,$_data){

        $jobPosition['name'] = $_data['name'];
        $jobPosition['department_id'] = $_data['department_id'];
        $jobPosition['location_id'] = $_data['location_id'];
        $jobPosition['employee_type_id'] = $_data['employee_type_id'];
        $jobPosition['is_published'] = $_data['is_published'];
        $jobPosition['target'] = $_data['target'];
        $jobPosition['recruiter_id'] = $_data['recruiter_id'];
        $jobPosition['intervieews_ids'] = $_data['intervieews_ids'];
        $jobPosition['job_summary'] = $_data['job_summary'];
        $jobPosition['tenant_id'] = $_data['tenant_id'];
        $jobPosition['company_id'] = $_data['company_id'];
        $jobPosition['branch_id'] = $_data['branch_id'];
        $jobPosition['created_by'] = $_data['created_by'];

        $result = HrJobPositionModel::find($id)->update($jobPosition);
        if($result)
            return $result;
        else
            return false;

    }

    public function getByTenant($_data) {

        $tenant_id = $_data['tenant_id'];
        $company_id = $_data['company_id'];
        $branch_id = $_data['branch_id'];

        $result = HrJobPositionModel::with('recruiter','location','department','employee_types')->where([
            'tenant_id' => $tenant_id,
            'company_id' => $company_id,
            'branch_id' => $branch_id,
        ])->orderBy('id','desc')->get();

        if($result)
            return $result;
        else
            return false;
    }

    public function delete($id){
        $result =  HrJobPositionModel::find($id);
        if($result){
            return $result->delete();
        }
        return false;
    }

}
