<?php

namespace App\Services;

use App\Models\HrLocationModel;
use App\Services\HrAddressService;

class HrLocationService {

    public function addNew($_data)
    {
        $tenant_id = $_data['tenant_id'];
        $company_id = $_data['company_id'];
        $branch_id = $_data['branch_id'];
        $created_by = $_data['created_by'];

        $addressService = new HrAddressService();
        $address = $addressService->addNew($_data);
        if($address){
            $location['address_id'] = $address->id;
            $location['name'] = $_data['location_name'];
            $location['have_fingerprint'] = $_data['location_have_fingerprint'];
            $location['ip_fingerprint'] = $_data['location_ip_fingerprint'];
            $location['description'] = $_data['location_description'];
            $location['tenant_id'] = $tenant_id;
            $location['company_id'] = $company_id;
            $location['branch_id'] = $branch_id;
            $location['created_by'] = $created_by;

            $added_location= HrLocationModel::create($location);

            if($added_location){
                return $added_location;
            }
        }
        return false;
    }
    public function updateLocation($id,$_data)
    {
        $tenant_id = $_data['tenant_id'];
        $company_id = $_data['company_id'];
        $branch_id = $_data['branch_id'];
        $created_by = $_data['created_by'];

        $location['name'] = $_data['location_name'];
        $location['have_fingerprint'] = $_data['location_have_fingerprint'];
        $location['ip_fingerprint'] = $_data['location_ip_fingerprint'];
        $location['description'] = $_data['location_description'];

        $addressService = new HrAddressService();
        $updateAddress = $addressService->updateAddress($_data['address_id'],$_data);

        $location['tenant_id'] = $tenant_id;
        $location['company_id'] = $company_id;
        $location['branch_id'] = $branch_id;
        $location['created_by'] = $created_by;

        $added_location= HrLocationModel::find($id);

        if($added_location){
            $updated = $added_location->update($location);
            return $updated;
        }

        return false;
    }

    public function getByTenant($tenantId,$companyId,$branchId){
        return HrLocationModel::with('addressRelation.countryRelation')->where([
            'tenant_id' => $tenantId,
            'company_id' => $companyId,
            'branch_id' => $branchId
        ])->orderBY('id','desc')->get();
    }

    public function delete($id){
        $result =  HrLocationModel::find($id);
        if($result){
            return $result->delete();
        }
        return false;
    }

}

