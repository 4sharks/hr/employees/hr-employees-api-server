<?php
namespace App\Services;

use App\Models\HrCountryModel;

class HrCountryService {

    public function getAll(){
        return HrCountryModel::get();
    }

}
