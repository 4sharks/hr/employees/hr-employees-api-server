<?php
namespace App\Services;

use App\Classes\Employee;
use App\Models\HrEmployeeConfigModel;
use App\Models\HrEmployeeModel;
use App\Models\HrEmployeePrivateInfoModel;
use App\Models\HrEmployeeResumeSkill;
use App\Models\HrEmployeesResumeLine;
use App\Models\HrEmployeeWorkPermit;

class HrEmployeeService {

    public function addNew($_data) {
        if($_data['emp_no']){
            $check_available = HrEmployeeModel::where([
                'emp_no' => $_data['emp_no'],
                'tenant_id' => $_data['tenant_id'],
                'company_id' => $_data['company_id'],
                'branch_id' => $_data['branch_id']
            ])->count();
            if($check_available > 0){
                return [
                    "status" => 0,
                    "message" =>"employee number is not available"
                ];
            }
        }
        $emp = new Employee();
        $employee = [
            'emp_no' => $_data['emp_no'],
            'name_en' => $_data['name_en'],
            'name_ar' => $_data['name_ar'],
            'job_position_id' => $_data['job_position_id'],
            'employee_type_id' => $_data['employee_type_id'],
            'tags' => $_data['tags'],
            'department_id' => $_data['department_id'],
            'manager_id' => $_data['manager_id'],
            'coach_id' => $_data['coach_id'],
            'pic' => $_data['pic'],
            'work_phone' => $_data['work_phone'],
            'work_mobile' => $_data['work_mobile'],
            'work_email' => $_data['work_email'],
            'work_location_id' => $_data['work_location_id'],
            'approvers_expense' => $_data['approvers_expense'],
            'approvers_timeoff' => $_data['approvers_timeoff'],
            'working_hours_id' => $_data['working_hours_id'],
            'timezone' => $_data['timezone'],
            'related_user_id' => $_data['related_user_id'],
            'current_contract_id' => $_data['current_contract_id'],
            'pin_code' => $_data['pin_code'],
            'badge_id' => $_data['badge_id'],
            'tenant_id' => $_data['tenant_id'],
            'company_id' => $_data['company_id'],
            'branch_id' => $_data['branch_id'],
            'created_by' => $_data['created_by'],
        ];

        $emp->setNewEmployee($employee);

        $added = HrEmployeeModel::create($emp->getEmployee());
        if($added){
            $emp_id = $added['id'] ;
            if($_data['private_info']){
                $private_info = [
                    "emp_id"    => $emp_id,
                    "email"     => $_data['private_info']['email'],
                    "phone"     => $_data['private_info']['phone'],
                    "emergency_person_name" => $_data['private_info']['emergency_person_name'],
                    "emergency_person_phone" => $_data['private_info']['emergency_person_phone'],
                    "gender"    => $_data['private_info']['gender'],
                    "nationality_id" => $_data['private_info']['nationality_id'],
                    "identification_id" => $_data['private_info']['identification_id'],
                    "passport_id" => $_data['private_info']['passport_id'],
                    "dob" => $_data['private_info']['dob'],
                    "place_of_birth" => $_data['private_info']['place_of_birth'],
                    "country_of_birth" => $_data['private_info']['country_of_birth'],
                    "home_address_id" => $_data['private_info']['home_address_id'],
                    "home_work_distance" => $_data['private_info']['home_work_distance'],
                    "marital_status" => $_data['private_info']['marital_status'],
                    "number_of_children" => $_data['private_info']['number_of_children'],
                    "bank_account_id"   => $_data['private_info']['bank_account_id'],
                    "main_language" => $_data['private_info']['main_language'],
                    "certificate_level_id" => $_data['private_info']['certificate_level_id'],
                    "field_of_study" => $_data['private_info']['field_of_study'],
                    "school_name"   => $_data['private_info']['school_name'],
                ];
                $add_private_info = HrEmployeePrivateInfoModel::create($private_info);
                if($add_private_info){
                    $added['private_info'] = $add_private_info;
                }
            }
            if($_data['work_permits']){
                $add_work_permit = HrEmployeeWorkPermit::create([
                    "emp_id" => $emp_id,
                    "visa_no" => $_data['work_permits']['visa_no'],
                    "visa_permit_no" => $_data['work_permits']['visa_permit_no'],
                    "visa_expire_date" => $_data['work_permits']['visa_expire_date'],
                    "work_permit_expire_date" => $_data['work_permits']['work_permit_expire_date'],
                    "work_permit_file" => $_data['work_permits']['work_permit_file'],
                    "created_by" => $_data['created_by']
                ]);
                if($add_work_permit){
                    $added['work_permit'] = $add_work_permit;
                }
            }
            if($_data['resume_lines']){
                $_lines = [];
                foreach($_data['resume_lines'] as $line) {
                    $_lines[] = HrEmployeesResumeLine::create([
                        "emp_id" => $emp_id,
                        "title"  => $line['title'],
                        "type"   => $line['type'],
                        "date_start" => $line['date_start'],
                        "date_end"   => $line['date_end'],
                        "description" => $line['description'],
                        'tenant_id' => $_data['tenant_id'],
                        'company_id' => $_data['company_id'],
                        'created_by' => $_data['created_by']
                    ]);
                }
                $added['resume_lines'] = $_lines;
            }
            if($_data['resume_skills']){
                $_skills = [];
                foreach($_data['resume_skills'] as $skill){
                    $_skills[] = HrEmployeeResumeSkill::create([
                        "emp_id" => $emp_id,
                        "skill_type_id" => $skill['skill_type_id'],
                        "skill_id" => $skill['skill_id'],
                        "skill_level_id" => $skill['skill_level_id'],
                        'tenant_id' => $_data['tenant_id'],
                        'company_id' => $_data['company_id'],
                        'created_by' => $_data['created_by']
                    ]);
                }
                $added['resume_skills'] = $_skills;
            }
            if($_data['employee_config']){
                $config = HrEmployeeConfigModel::create([
                    "emp_id" => $emp_id,
                    "emp_type" => $_data['employee_config']["emp_type"],
                    "emp_related_user_id" => $_data['employee_config']["emp_related_user_id"],
                    "current_contract_id" => $_data['employee_config']["current_contract_id"],
                    "pincode" => $_data['employee_config']["pincode"],
                    "badge_id" => $_data['employee_config']["badge_id"],
                    "is_manager" => $_data['employee_config']["is_manager"],
                    "is_trainer" => $_data['employee_config']["is_trainer"],
                    "is_approval_expense" => $_data['employee_config']["is_approval_expense"],
                    "is_approval_timeoff" => $_data['employee_config']["is_approval_timeoff"],
                ]);
                if($config){
                    $added['employee_config']  = $config;
                }
            }

            return [
                "status" =>1,
                "message" =>"employee added successfully",
                "data" => $added
            ];
        }
        return [
            "status" => 0,
            "message" =>"Error adding employee"
        ];
    }
    public function update($id,$_data) {
        if(!$id){
            return [
                "status" => 0,
                "message" =>"Employee ID does not exist"
            ];
        }
        $emp = new Employee();
        $employee = [
            'emp_no' => $_data['emp_no'],
            'name_en' => $_data['name_en'],
            'name_ar' => $_data['name_ar'],
            'job_position_id' => $_data['job_position_id'],
            'employee_type_id' => $_data['employee_type_id'],
            'tags' => $_data['tags'],
            'department_id' => $_data['department_id'],
            'manager_id' => $_data['manager_id'],
            'coach_id' => $_data['coach_id'],
            'pic' => $_data['pic'],
            'work_phone' => $_data['work_phone'],
            'work_mobile' => $_data['work_mobile'],
            'work_email' => $_data['work_email'],
            'work_location_id' => $_data['work_location_id'],
            'approvers_expense' => $_data['approvers_expense'],
            'approvers_timeoff' => $_data['approvers_timeoff'],
            'working_hours_id' => $_data['working_hours_id'],
            'timezone' => $_data['timezone'],
            'related_user_id' => $_data['related_user_id'],
            'current_contract_id' => $_data['current_contract_id'],
            'pin_code' => $_data['pin_code'],
            'badge_id' => $_data['badge_id'],
            'tenant_id' => $_data['tenant_id'],
            'company_id' => $_data['company_id'],
            'branch_id' => $_data['branch_id'],
            'created_by' => $_data['created_by'],
        ];

        $emp->setNewEmployee($employee);
        $emp_id = $id ;

        $empUpdated = HrEmployeeModel::find($emp_id);
        if($empUpdated){
            $empUpdated->update($emp->getEmployee());
        }
        if($_data['private_info']){
            $private_info = [
                "email"     => $_data['private_info']['email'],
                "phone"     => $_data['private_info']['phone'],
                "emergency_person_name" => $_data['private_info']['emergency_person_name'],
                "emergency_person_phone" => $_data['private_info']['emergency_person_phone'],
                "gender"    => $_data['private_info']['gender'],
                "nationality_id" => $_data['private_info']['nationality_id'],
                "identification_id" => $_data['private_info']['identification_id'],
                "passport_id" => $_data['private_info']['passport_id'],
                "dob" => $_data['private_info']['dob'],
                "place_of_birth" => $_data['private_info']['place_of_birth'],
                "country_of_birth" => $_data['private_info']['country_of_birth'],
                "home_address_id" => $_data['private_info']['home_address_id'],
                "home_work_distance" => $_data['private_info']['home_work_distance'],
                "marital_status" => $_data['private_info']['marital_status'],
                "number_of_children" => $_data['private_info']['number_of_children'],
                "bank_account_id"   => $_data['private_info']['bank_account_id'],
                "main_language" => $_data['private_info']['main_language'],
                "certificate_level_id" => $_data['private_info']['certificate_level_id'],
                "field_of_study" => $_data['private_info']['field_of_study'],
                "school_name"   => $_data['private_info']['school_name'],
            ];
            $update_private_info = HrEmployeePrivateInfoModel::where([
                "emp_id"    => $emp_id,
            ])->first();
            if($update_private_info){
                $update_private_info->update($private_info);
            }else{
                $private_info['emp_id'] = $emp_id;
                HrEmployeePrivateInfoModel::create($private_info);
            }

        }
        if($_data['work_permits']){
            $update_work_permit = HrEmployeeWorkPermit::where([
                "emp_id" => $emp_id,
            ])->first();
            if($update_work_permit){
                $data_work_permit = [
                    "visa_no" => $_data['work_permits']['visa_no'],
                    "visa_permit_no" => $_data['work_permits']['visa_permit_no'],
                    "visa_expire_date" => $_data['work_permits']['visa_expire_date'],
                    "work_permit_expire_date" => $_data['work_permits']['work_permit_expire_date'],
                    "work_permit_file" => $_data['work_permits']['work_permit_file'],
                    "created_by" => $_data['created_by']
                ];
                $update_work_permit->update($data_work_permit);
            }else{
                $data_work_permit['emp_id'] = $emp_id;
                HrEmployeeWorkPermit::create($data_work_permit);
            }
        }
        if($_data['resume_lines']){
            $_lines = [];
            HrEmployeesResumeLine::where([
                "emp_id" => $emp_id,
            ])->delete();
            foreach($_data['resume_lines'] as $line) {
                $_lines[] = HrEmployeesResumeLine::create([
                    "emp_id" => $emp_id,
                    "title"  => $line['title'] ,
                    "type"   => $line['type'],
                    "date_start" => $line['date_start']  ,
                    "date_end"   => $line['date_end'] ,
                    "description" => $line['description'] ,
                    'tenant_id' => $_data['tenant_id'],
                    'company_id' => $_data['company_id'],
                    'created_by' => $_data['created_by']
                ]);
            }
        }
        if($_data['resume_skills']){
            $_skills = [];
            HrEmployeeResumeSkill::where([
                "emp_id" => $emp_id,
            ])->delete();
            foreach($_data['resume_skills'] as $skill){
                $_skills[] = HrEmployeeResumeSkill::create([
                    "emp_id" => $emp_id,
                    "skill_type_id" => $skill['skill_type_id'],
                    "skill_id" => $skill['skill_id'],
                    "skill_level_id" => $skill['skill_level_id'],
                    'tenant_id' => $_data['tenant_id'],
                    'company_id' => $_data['company_id'],
                    'created_by' => $_data['created_by']
                ]);
            }
        }
        if($_data['employee_config']){
            $config = HrEmployeeConfigModel::where([
                "emp_id" => $emp_id,
            ])->first();
            $data_config =[
                "emp_type" => $_data['employee_config']["emp_type"],
                "emp_related_user_id" => $_data['employee_config']["emp_related_user_id"],
                "current_contract_id" => $_data['employee_config']["current_contract_id"],
                "pincode" => $_data['employee_config']["pincode"],
                "badge_id" => $_data['employee_config']["badge_id"],
                "is_manager" => $_data['employee_config']["is_manager"],
                "is_trainer" => $_data['employee_config']["is_trainer"],
                "is_approval_expense" => $_data['employee_config']["is_approval_expense"],
                "is_approval_timeoff" => $_data['employee_config']["is_approval_timeoff"],
            ];
            if($config){
                $config->update($data_config);
            }else{
                $data_config['emp_id'] = $emp_id;
                HrEmployeeConfigModel::create($data_config);
            }
        }

        return [
            "status" =>1,
            "message" =>"Employee updated successfully",
            "data" => $empUpdated
        ];

        return [
            "status" => 0,
            "message" =>"Error updating employee"
        ];
    }

    public function getByTenant($tenantId,$companyId,$branchId){
        $result = HrEmployeeModel::with('departmentRelation')->where([
            'tenant_id' => $tenantId,
            'company_id' => $companyId,
            'branch_id' => $branchId
        ])->orderBy('id','desc')->get();
        return $result;
    }
    public function getById($tenantId,$companyId,$branchId,$empId){
        $result = HrEmployeeModel::with([
            'departmentRelation',
            'job_position',
            'manager:id,name_ar,name_en',
            'coach:id,name_ar,name_en',
            'approversExpense:id,name_ar,name_en',
            'approversTimeoff:id,name_ar,name_en',
            'work_location',
            'working_hours',
            'timeZone',
            'private_info.nationality',
            'work_permit',
            'resume_lines',
            'resume_skills',
            'resume_skills.type',
            'resume_skills.skills',
            'resume_skills.levels',
            'config'
            ])->where([
            'tenant_id' => $tenantId,
            'company_id' => $companyId,
            'branch_id' => $branchId,
            'id' => $empId
        ])->orderBy('id','desc')->first();
        return $result;
    }
    public function getByUserId($userId){
        $result = HrEmployeeModel::with([
            'departmentRelation:id,name,parent_id,manager_id',
            'job_position:id,name,department_id,location_id,employee_type_id,is_published,target,recruiter_id,intervieews_ids,job_summary',
            'manager:id,name_ar,name_en',
            'coach:id,name_ar,name_en',
            'approversExpense:id,name_ar,name_en',
            'approversTimeoff:id,name_ar,name_en',
            'work_location.addressRelation:id,type,ref_id,country_id,city,region,street,post_code,lat,lng,description',
            'working_hours:id,label,value,lang',
            'timeZone',
            'private_info.nationality',
            'work_permit',
            'resume_lines',
            'resume_skills',
            'resume_skills.type',
            'resume_skills.skills',
            'resume_skills.levels',
            'config'
            ])->where([
            'related_user_id' => $userId
        ])->orderBy('id','desc')->first();
        return $result;
    }

    public function getByDept($tenantId,$companyId,$branchId,$departmentId){

        $where = [
            'tenant_id' => $tenantId,
            'company_id' => $companyId,
            'branch_id' => $branchId
        ];

        if($departmentId != 0){
            $where['department_id'] = $departmentId;
        }
        $result = HrEmployeeModel::with('departmentRelation')->where($where)->orderBy('id','desc')->get();
        return $result;
    }

    public function delete($id){
        $result =  HrEmployeeModel::find($id);
        if($result){
            return $result->delete();
        }
        return false;
    }

}
