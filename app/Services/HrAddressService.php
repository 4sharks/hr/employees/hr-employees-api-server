<?php
namespace App\Services;

use App\Models\HrAddressModel;


class HrAddressService {

    public function addNew($_data){
        $tenant_id = $_data['tenant_id'];
        $company_id = $_data['company_id'];
        $branch_id = $_data['branch_id'];
        $created_by = $_data['created_by'];

        $address['type'] = $_data['address_type'];
        $address['ref_id'] = $_data['address_ref_id'];
        $address['country_id'] = $_data['address_country_id'];
        $address['city'] = $_data['address_city'];
        $address['region'] = $_data['address_region'];
        $address['street'] = $_data['address_street'];
        $address['post_code'] = $_data['address_post_code'];
        $address['lat'] = $_data['address_lat'];
        $address['lng'] = $_data['address_lng'];
        $address['description'] = $_data['address_description'];
        $address['tenant_id'] = $tenant_id;
        $address['company_id'] = $company_id;
        $address['branch_id'] = $branch_id;
        $address['created_by'] = $created_by;

        return HrAddressModel::create($address);
    }

    public function updateAddress($id,$_data){
        $tenant_id = $_data['tenant_id'];
        $company_id = $_data['company_id'];
        $branch_id = $_data['branch_id'];
        $created_by = $_data['created_by'];

        $address['type'] = $_data['address_type'];
        $address['ref_id'] = $_data['address_ref_id'];
        $address['country_id'] = $_data['address_country_id'];
        $address['city'] = $_data['address_city'];
        $address['region'] = $_data['address_region'];
        $address['street'] = $_data['address_street'];
        $address['post_code'] = $_data['address_post_code'];
        $address['lat'] = $_data['address_lat'];
        $address['lng'] = $_data['address_lng'];
        $address['description'] = $_data['address_description'];
        $address['tenant_id'] = $tenant_id;
        $address['company_id'] = $company_id;
        $address['branch_id'] = $branch_id;
        $address['created_by'] = $created_by;

        return HrAddressModel::find($id)->update($address);
    }
}
