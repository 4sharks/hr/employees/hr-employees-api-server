<?php
namespace App\Services;

use App\Models\HrEmployeeTypeModel;

class HrEmployeeTypesService{

    public function getAll(){
        $result = HrEmployeeTypeModel::orderBy('id','desc')->get();
        return $result;
    }

}
