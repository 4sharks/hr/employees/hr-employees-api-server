<?php
namespace App\Services;

use App\Models\HrDepartmentModel;

class HrDepartmentService {

    public function addNew($_data){
        // $tenant_id = $_data['tenant_id'];
        // $company_id = $_data['company_id'];
        // $branch_id = $_data['branch_id'];
        // $created_by = $_data['created_by'];

        // $name = $_data['name'];
        // $parent_id = $_data['parent_id'] || null;
        // $manager_id = $_data['manager_id'] || null;

        return HrDepartmentModel::create($_data);

    }
    public function updateRow($id,$_data){

        return HrDepartmentModel::find($id)->update($_data);

    }

    public function getByTenant($tenantId,$companyId,$branchId){
        return HrDepartmentModel::with('parentRelation.parentRelation.parentRelation:id,name','managerRelation:id,name_en,name_ar')->where([
            'tenant_id' => $tenantId,
            'company_id' => $companyId,
            'branch_id' => $branchId
        ])->orderBY('id','desc')->get();
    }

    public function delete($id){
        $result =  HrDepartmentModel::find($id);
        if($result){
            return $result->delete();
        }
        return false;
    }

}
